/*!
    * Start Bootstrap - Resume v6.0.1 (https://startbootstrap.com/template-overviews/resume)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-resume/blob/master/LICENSE)
    */
    (function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#sideNav",
    });
})(jQuery); // End of use strict

document.addEventListener("DOMContentLoaded", function (event) {
    //Have some variables
    var storeName = "";
    var StartStoreCode = "000000";
    var priceCheck = 0;
    var price = 0;
    var branchCode = 2981; //4181, 4171, 2511 //lidl, 1414 tesco
    const ConstOne = "3";
    var dayCode = "45";
    var yearCode = new Date().getFullYear().toString().substr(2, 2);
    var receiptNumber = 0; //varies per store chain.

    //Set trigger element click event listener
    var triggerElement = document.body.getElementsByClassName('input_spoustec')[0];
    triggerElement.addEventListener('click', function () {

        //read form values
        storeName = document.querySelector('input[name = "store"]:checked').value;      //Store code
        StartStoreCode = storeCodeFromName(storeName);

        var input_lahve3 = document.getElementById("input_lahve3").value;
        var input_prepravky = document.getElementById("input_prepravky").value;

        var input_hlavicka1 = document.getElementById("input_hlavicka1").value;
        document.getElementById("hlavicka1").innerHTML = input_hlavicka1;

        var input_hlavicka2 = document.getElementById("input_hlavicka2").value;
        document.getElementById("hlavicka2").innerHTML = input_hlavicka2;

        var input_hlavicka3 = document.getElementById("input_hlavicka3").value;
        document.getElementById("hlavicka3").innerHTML = input_hlavicka3;

        var input_hlavicka4 = document.getElementById("input_hlavicka4").value;
        if (!input_hlavicka4) { input_hlavicka4 = "<br>"; }
        document.getElementById("hlavicka4").innerHTML = input_hlavicka4;

        var input_listek = document.getElementById("input_listek").value;
        document.getElementById("listek").innerHTML = input_listek + "&nbsp;";
    })
})