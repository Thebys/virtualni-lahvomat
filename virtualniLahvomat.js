const ConstOne = "3";
var input_lahve3 = 0;
var input_prepravky = 0;
var basePrice = 0;

var storeName = "";
var StartStoreCode = "000000";
var priceCheck = 0;
var branchCode = 2021; //4181, 4171, 2511 //lidl, 1414 tesco
// Lidl = 2021
var dayCode = "45";
var yearCode = new Date().getFullYear().toString().substr(2, 2);
var receiptNumber = 0; //varies per store chain.

var price = 0;
var input_hlavicka1 = "";
var input_hlavicka2 = "";
var input_hlavicka3 = "";
var input_hlavicka4 = "";
var input_listek = "";
var input_paticka1 = "";
var input_paticka2 = "";

function CalculateEanCheckDigit(inputData) {
    var sude = 0;
    var liche = 0;
    var input = inputData;
    var mod = 0;

    for (i = 1; i < input.length + 1; i++) {
        if (i % 2 == 0) sude += parseInt(input.charAt(i - 1), 10);
        else liche += parseInt(input.charAt(i - 1), 10);
    }

    sude = sude * 3;
    mod = Math.ceil((liche + sude) / 10) * 10;
    var result = mod - (liche + sude);

    return result;
}

function CalculatePriceCheckDigit(input) {

    var d1p = factor5PlusDigitToProduct(parseInt(input.charAt(0), 10));
    var d2p = factor2DigitToProduct(parseInt(input.charAt(1), 10));
    var d3p = factor5MinusDigitToProduct(parseInt(input.charAt(2), 10));
    var d4p = factor5PlusDigitToProduct(parseInt(input.charAt(3), 10));
    var d5p = factor2DigitToProduct(parseInt(input.charAt(4), 10));

    var sum = d1p + d2p + d3p + d4p + d5p;
    ceil = Math.ceil((sum) / 10) * 10;
    var result = factor5MinusProductToDigit(ceil - sum);

    return result;
}

function factor2DigitToProduct(digit) {
    switch (digit) {
        case 0:
            return 0;
        case 1:
            return 2;
        case 2:
            return 4;
        case 3:
            return 6;
        case 4:
            return 8;
        case 5:
            return 9;
        case 6:
            return 1;
        case 7:
            return 3;
        case 8:
            return 5;
        case 9:
            return 7;
    }
}
function factor5PlusDigitToProduct(digit) {
    switch (digit) {
        case 0:
            return 0;
        case 1:
            return 5;
        case 2:
            return 1;
        case 3:
            return 6;
        case 4:
            return 2;
        case 5:
            return 7;
        case 6:
            return 3;
        case 7:
            return 8;
        case 8:
            return 4;
        case 9:
            return 9;
    }
}
function factor5MinusDigitToProduct(digit) {
    switch (digit) {
        case 0:
            return 0;
        case 1:
            return 5;
        case 2:
            return 9;
        case 3:
            return 4;
        case 4:
            return 8;
        case 5:
            return 3;
        case 6:
            return 7;
        case 7:
            return 2;
        case 8:
            return 6;
        case 9:
            return 1;
    }
}
function factor5MinusProductToDigit(product) {
    switch (product) {
        case 0:
            return 0;
        case 1:
            return 9;
        case 2:
            return 7;
        case 3:
            return 5;
        case 4:
            return 3;
        case 5:
            return 1;
        case 6:
            return 8;
        case 7:
            return 6;
        case 8:
            return 4;
        case 9:
            return 2;
    }
}

function storeCodeFromName(storeName) {
    const StoreCodeTesco = "249999";
    const StoreCodePenny = "213333";
    const StoreCodeBilla = "244444";
    const StoreCodeLidl = "2010";

    switch (storeName) {
        case "penny":
            return StoreCodePenny;
        case "tesco":
            return StoreCodeTesco;
        case "billa":
            return StoreCodeBilla;
        case "lidl":
            return StoreCodeLidl;
    }
}
function trimLineWhiteSpace(lineText, desiredLength) {
    var attemptRemovalAt = 0;

    while (lineText.length > desiredLength) {
        var position = lineText.length - attemptRemovalAt;
        if (lineText.charAt(position) == " ") {
            lineText = lineText.slice(0, position) + lineText.slice(position + 1);
        }
        else { attemptRemovalAt += 1; }
    }
    return lineText;
}

function logUsage(context) {
    loadInputFromForm();
    window.dataLayer = window.dataLayer || []
    window.dataLayer.push({
        event: context,
        h1_storeBrand: input_hlavicka1,
        h2_storeId: input_hlavicka2,
        h3_sotreTown: input_hlavicka3,
        h4_storeService: input_hlavicka4,
        ticketId: input_listek,
        branchCode: input_branchCode,
        f1_machineId: input_paticka1,
        f2_timeStamp: input_paticka2,
        lahve: input_lahve3,
        prepravky: input_prepravky,
        ticketValue: price/100
    })
}


function loadInputFromForm() {
    //read form values
    storeName = document.querySelector('input[name = "store"]:checked').value;      //Store code
    StartStoreCode = storeCodeFromName(storeName);

    branchCode = document.getElementById("input_branchCode").value;
    
    input_lahve3 = document.getElementById("input_lahve3").value;
    input_prepravky = document.getElementById("input_prepravky").value;

    input_hlavicka1 = document.getElementById("input_hlavicka1").value;
    document.getElementById("hlavicka1").innerHTML = input_hlavicka1;

    input_hlavicka2 = document.getElementById("input_hlavicka2").value;
    document.getElementById("hlavicka2").innerHTML = input_hlavicka2;

    input_hlavicka3 = document.getElementById("input_hlavicka3").value;
    document.getElementById("hlavicka3").innerHTML = input_hlavicka3;

    input_hlavicka4 = document.getElementById("input_hlavicka4").value;
    if (!input_hlavicka4) { input_hlavicka4 = "<br>"; }
    document.getElementById("hlavicka4").innerHTML = input_hlavicka4;

    input_listek = document.getElementById("input_listek").value;
    document.getElementById("listek").innerHTML = input_listek + "&nbsp;";

    input_paticka1 = document.getElementById("input_paticka1").value;
    document.getElementById("paticka1").innerHTML = input_paticka1;

    input_paticka2 = document.getElementById("input_paticka2").value;
    document.getElementById("paticka2").innerHTML = input_paticka2;

    input_paticka3 = document.getElementById("input_paticka3").value;
    document.getElementById("paticka3").innerHTML = input_paticka3;
}
function prepareForm() {
    //Update form based on defaults:
    document.getElementById("input_lahve3").value = input_lahve3;
    document.getElementById("input_prepravky").value = input_prepravky;

    document.getElementById("input_hlavicka1").value = input_hlavicka1;
    document.getElementById("input_hlavicka2").value = input_hlavicka2;
    document.getElementById("input_hlavicka3").value = input_hlavicka3;
    document.getElementById("input_hlavicka4").value = input_hlavicka4;

    document.getElementById("input_listek").value = input_listek;
    document.getElementById("input_branchCode").value = input_branchCode;

    document.getElementById("input_paticka1").value = input_paticka1;
    document.getElementById("input_paticka2").value = input_paticka2;
}

function calculateTicketPrice() {
    basePrice = (input_lahve3 * 3) + (input_prepravky * 100);
    price = basePrice * 10;

    if (storeName == "penny") {
        price = price * 10; //Penny mode is price *10 from normal code.
    }
    if (storeName == "lidl") {
        price = price * 10; //lidl mode is price *10 from normal code and 6 digits.
        price = ("000000" + price).substr(-6, 6);
    }
    else {
        price = ("00000" + price).substr(-5, 5);
    }
}
function printTicket() {
    prepareTicket();
    logUsage("printTicket");
    this.window.print();
}
function generateTicket() {
    prepareTicket();
    logUsage("generateTicket");
}
function prepareTicket() {
    loadInputFromForm();

    //calculate price
    calculateTicketPrice();

    //Remove all classes from .ticket and add .ticket .storecodename
    document.querySelector('.ticket').className = 'ticket ' + storeName;

    //update the ticket
    if(storeName == "lidl"){
        document.getElementById("branchCodeDiv").style.display = "block";
        document.getElementById("prouzek").innerHTML = "<span class=\"currency\">Kč  </span>" + basePrice + ".00&nbsp;";
    }
    else{
        document.getElementById("branchCodeDiv").style.display = "none";
        document.getElementById("prouzek").innerHTML = basePrice + ".00&nbsp;";
    }

    var lahveRadekText = trimLineWhiteSpace(input_lahve3 + "x 3.0 Pivo 0,50             " + (input_lahve3 * 3) + ".0", 32);
    var lahveCelkemText = trimLineWhiteSpace(input_lahve3 + "x                           " + (input_lahve3 * 3) + ".0", 32);
    document.getElementById("lahve3").innerHTML = "<pre>" + lahveRadekText + "</pre>";
    document.getElementById("lahveCelkem").innerHTML = "<pre>" + lahveCelkemText + "</pre>";

    var prepravkyRadekText = trimLineWhiteSpace(input_prepravky + "x prepravka                 " + (input_prepravky * 100) + ".0", 32);
    var prepravkyCelkemText = trimLineWhiteSpace(input_prepravky + "x                           " + (input_prepravky * 100) + ".0", 32);
    document.getElementById("prepravkyRadek").innerHTML = "<pre>" + prepravkyRadekText + "</pre>";
    document.getElementById("prepravkyCelkem").innerHTML = "<pre>" + prepravkyCelkemText + "</pre>";

    //determine and generate code
    var codeType;
    if (storeName == "tesco" || storeName == "lidl") {
        codeType = "code128";
    } else {
        codeType = "ean";
    }

    if (codeType == "ean") {
        document.getElementById("barcodeText").style.display = "block";
        document.getElementById("barcodeHolder").style.textAlign = "center";
        priceCheck = CalculatePriceCheckDigit(price);
        var valueToEncode = StartStoreCode + "" + priceCheck + "" + price + "";
        var checkDigit = CalculateEanCheckDigit(valueToEncode);
        var valueToEncode = valueToEncode + "" + checkDigit;
        var valueToDisplay = valueToEncode.substring(0, 1) + " " + valueToEncode.substring(1);
        var valueToDisplay = valueToDisplay.substring(0, 8) + " " + valueToDisplay.substring(8);
        document.getElementById("barcodeText").innerHTML = valueToDisplay;
        JsBarcode("#barcode", valueToEncode, {
            format: "EAN13",
            displayValue: false,
            flat: true,
            width: 1.80,
            height: 54,
            margin: 0,
            lineColor: "rgba(6, 6, 6, 0.85)"
        });
    }

    if (codeType == "code128") {
        document.getElementById("barcodeText").style.display = "none";
        document.getElementById("barcodeHolder").style.textAlign = "center";
        if (storeName == "tesco") {
            receiptNumber = "987";
            priceCheck = CalculatePriceCheckDigit(price);
            var valueToEncode = StartStoreCode + "" + priceCheck + "" + price + "" + branchCode + "" + ConstOne + "" + dayCode + "" + yearCode + "" + receiptNumber;
            var checkDigit = CalculateEanCheckDigit(valueToEncode);
            var valueToEncode = valueToEncode + "" + checkDigit;
            JsBarcode("#barcode", valueToEncode, {
                format: "CODE128",
                displayValue: false,
                width: 1,
                height: 100,
                margin: 0
            });
        }
        if (storeName == "lidl") {
                // Start of Selection
                receiptNumber = input_listek.match(/\d+/)[0];
            document.getElementById("barcodeHolder").style.textAlign = "left";
            var valueToEncode = "\xCF" + StartStoreCode + "" + branchCode + "" + receiptNumber + "" + price; // \xCF is GS-1 Indicator

            JsBarcode("#barcode", valueToEncode, {
                format: "CODE128",
                displayValue: false,
                width: 1,//0.92,
                height: 50,
                margin: 0
            });
        }

    }
}
function loadPenny() {
    input_hlavicka1 = "PENNY MARKET";
    input_hlavicka2 = "fil. 645, Horice";
    input_hlavicka3 = "Horice";
    input_hlavicka4 = "Výkup lahví";
    input_listek = "Listek 051583";
    input_paticka1 = "Tomra 705";
    input_paticka2 = "15:10 04-RIJ-2020";
    input_lahve3 = 6;
    input_prepravky = 2;
}

function loadBilla() {
    input_hlavicka1 = "BILLA";
    input_hlavicka2 = "Hradecka ul. 1141";
    input_hlavicka3 = "Horice";
    input_hlavicka4 = "Vykup lahvi";
    input_listek = "Listek 043227";
    input_paticka1 = "Tomra 605 915002368";
    input_paticka2 = "13:37 16-ZAR-2020";
    input_lahve3 = 40;
    input_prepravky = 3;
}

function loadLidl() {
    input_hlavicka1 = "Lidl Česká republika s.r.o.";
    input_hlavicka2 = "202_Brno_Palackého třída";
    input_hlavicka3 = "Výkup zálohovaných obalů";
    input_hlavicka4 = "";
    input_branchCode = 2021;
    input_listek = "Listek 24556";
    input_paticka1 = "Tomra 9";
    input_paticka2 = "606657-90360001-06608-00"; 
    input_paticka3 = "13:37 12-DUB-2024";
    input_lahve3 = 22;
    input_prepravky = 6;
}

document.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById('store1').addEventListener('change', function () {
        loadPenny();
        prepareForm();
        generateTicket();
    });
    document.getElementById('store2').addEventListener('change', function () {
        loadBilla();
        prepareForm();
        generateTicket();
    });
    document.getElementById('store3').addEventListener('change', function () {
        loadLidl();
        prepareForm();
        generateTicket();
    });

    generateTicket();
});

